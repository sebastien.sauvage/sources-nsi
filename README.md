---
title: Ressources en NSI
date: 18 novembre 2019
author: sebastien.sauvage@ac-lille.fr
graphics: oui
numbersections: no
header-includes: |
    \usepackage[a4paper,left=1cm,right=1cm,top=1.5cm,bottom=1cm,headsep=0.5cm,,footskip=0.5cm]{geometry}
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhf{}
    \renewcommand{\headrulewidth}{0.4pt}
    \renewcommand{\footrulewidth}{0.4pt}
    \lhead{NSI - 2019-2020}
    \rhead{Ressources en NSI}
    \cfoot{\thepage}
    \lfoot{S.SAUVAGE}
    \rfoot{}
---
# Livres  
PASQUET, Stéphane. *Numérique et Sciences Informatiques*. Nathan, 2019. 224p. ISBN 978-209-157465-3  
![Courverture](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fec56229aec51f1baff1d-185c3068e22352c56024573e929788ff.ssl.cf1.rackcdn.com%2Fattachments%2Flarge%2F5%2F9%2F6%2F006654596.jpg&f=1&nofb=1){height=20% width=20%}  

BAYS, Serge. *Numérique et Sciences Informatiques*. ellipses, 2019. 376p. ISBN 978-234-003172-2  
![Courverture](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.QVlgYAt1lRTgbmFdzjgL_QHaJW%26pid%3DApi&f=1){height=20% width=20%}  

BALABONSKI, Thibaut, CONCHON, Sylvain, FILLIATRE, Jean-Christophe, NGUYEN, Kim. *Numérique et Sciences Informatiques*. ellipses, 2019. 518p. ISBN 978-234-003364-1  
![Courverture](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.WBeIOYseGHfgfwv235W0PQAAAA%26pid%3DApi&f=1){height=20% width=20%}  

ADOBET, Céline, CONNAN, Guillaume, ROZSAVOLGYI, Gérard, SIGNAC, Laurent. *Numérique et Sciences Informatiques*. Hatier, 2019. 320p. ISBN 978-240-105230-7  
![Courverture](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.DW7megTGhvWplLgqRD28yQAAAA%26pid%3DApi&f=1){height=20% width=20%}  

\newpage
CANU, Cécile. *Numérique et Sciences Informatiques*. ellipses, 2019. ?p. ISBN 978-234-003178-4  
![Couverture](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.p_EcePunmuIlTK5WJGGJsgHaKK%26pid%3DApi&f=1){height=20% width=20%}  


**Pour la programmation en Python**  
*Je les utilisais pour l'algorithmique en Mathématiques ainsi qu'en ICN. Ils restent quelques exercices intéressants comme source d'application.*  

MAILLE, Vincent. *Les bases de l'algorithmique et de la programmation*. ellipses, 2015. 240p. ISBN 978-234-000460-3  
![Courverture](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages-eu.ssl-images-amazon.com%2Fimages%2FI%2F515oqAWYL2L.jpg&f=1&nofb=1){height=20% width=20%}  

MAILLE, Vincent. *Apprendre la programmation par le jeu*. ellipses, 2015. 240p. ISBN 978-234-000459-7  
![Courverture](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimages-eu.ssl-images-amazon.com%2Fimages%2FI%2F41AleZ09P4L._SX195_.jpg&f=1&nofb=1){height=20% width=20%}  
